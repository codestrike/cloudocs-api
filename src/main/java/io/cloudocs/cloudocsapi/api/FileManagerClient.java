package io.cloudocs.cloudocsapi.api;

import io.cloudocs.cloudocsapi.bean.TemplateRQ;
import io.cloudocs.cloudocsapi.bean.TemplateRS;
import io.cloudocs.cloudocsapi.model.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class FileManagerClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileManagerClient.class);
    private RestTemplate restTemplate;

    public FileManagerClient() {
        restTemplate = new RestTemplate();
    }

    public TemplateRS generate(TemplateRQ templateRQ) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-Type", "application/json");
        HttpEntity<TemplateRQ> httpEntity = new HttpEntity<>(templateRQ, httpHeaders);
        HttpEntity<TemplateRS> response = null;
        try {
            response = restTemplate.exchange(
                    FileManagerConstant.URL_TEMPLATES,
                    HttpMethod.POST, httpEntity, TemplateRS.class);
        } catch (Exception e) {
            LOGGER.error("getFilePath", e);
        }
        if (response != null) {
            return response.getBody();
        } else {
            return null;
        }
    }
}
