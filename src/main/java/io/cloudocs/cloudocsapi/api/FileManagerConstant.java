package io.cloudocs.cloudocsapi.api;

/**
 * Created by javofegus on 4/11/17.
 */
public class FileManagerConstant {
//    public static final String BASE_URL = "http://172.18.0.145:8080/filemanager/";
    public static final String BASE_URL = "http://40.71.192.54:8080/filemanager/";
    public static final String URL_TEMPLATES = BASE_URL + "templates";
}
