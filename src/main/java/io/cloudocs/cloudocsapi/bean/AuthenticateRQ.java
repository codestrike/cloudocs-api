package io.cloudocs.cloudocsapi.bean;

/**
 * Created by wmartinez on 11/4/17.
 */
public class AuthenticateRQ {

    private String email;
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
