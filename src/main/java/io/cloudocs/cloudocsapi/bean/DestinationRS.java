package io.cloudocs.cloudocsapi.bean;

import io.cloudocs.cloudocsapi.model.Document;

public class DestinationRS {

    private String id;
    private String destinationCode;
    private Document document;
    private String type;
    private String dependenceCode;
    private String dependence;
    private String Sender;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDestinationCode() {
        return destinationCode;
    }

    public void setDestinationCode(String destinationCode) {
        this.destinationCode = destinationCode;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDependenceCode() {
        return dependenceCode;
    }

    public void setDependenceCode(String dependenceCode) {
        this.dependenceCode = dependenceCode;
    }

    public String getDependence() {
        return dependence;
    }

    public void setDependence(String dependence) {
        this.dependence = dependence;
    }

    public String getSender() {
        return Sender;
    }

    public void setSender(String sender) {
        Sender = sender;
    }
}
