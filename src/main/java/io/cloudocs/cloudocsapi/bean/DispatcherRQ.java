package io.cloudocs.cloudocsapi.bean;

import io.cloudocs.cloudocsapi.model.Document;

public class DispatcherRQ {

    private Document document;
    private String destinationCode;

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    public String getDestinationCode() {
        return destinationCode;
    }

    public void setDestinationCode(String destinationCode) {
        this.destinationCode = destinationCode;
    }
}
