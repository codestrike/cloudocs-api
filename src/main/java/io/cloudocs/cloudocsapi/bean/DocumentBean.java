package io.cloudocs.cloudocsapi.bean;

import io.cloudocs.cloudocsapi.model.Sender;
import io.cloudocs.cloudocsapi.model.Template;

import java.util.Date;


public class DocumentBean {

    private String id;
    private String type;
    private String code;
    private String state;
    private String dependenceCode;
    private String dependence;
    private Sender sender;
    private String fileName;
    private TemplateBean templateBean;
    private String subject;
    private Date fecha;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDependenceCode() {
        return dependenceCode;
    }

    public void setDependenceCode(String dependenceCode) {
        this.dependenceCode = dependenceCode;
    }

    public String getDependence() {
        return dependence;
    }

    public void setDependence(String dependence) {
        this.dependence = dependence;
    }

    public Sender getSender() {
        return sender;
    }

    public void setSender(Sender sender) {
        this.sender = sender;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public TemplateBean getTemplateBean() {
        return templateBean;
    }

    public void setTemplateBean(TemplateBean templateBean) {
        this.templateBean = templateBean;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
}
