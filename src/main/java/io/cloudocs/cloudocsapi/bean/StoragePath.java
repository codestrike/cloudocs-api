package io.cloudocs.cloudocsapi.bean;

/**
 * Created by javofegus on 4/11/17.
 */
public class StoragePath {
    private String parentPath;
    private String docFile;
    private String pdfFile;

    public String getParentPath() {
        return parentPath;
    }

    public void setParentPath(String parentPath) {
        this.parentPath = parentPath;
    }

    public String getDocFile() {
        return docFile;
    }

    public void setDocFile(String docFile) {
        this.docFile = docFile;
    }

    public String getPdfFile() {
        return pdfFile;
    }

    public void setPdfFile(String pdfFile) {
        this.pdfFile = pdfFile;
    }
}
