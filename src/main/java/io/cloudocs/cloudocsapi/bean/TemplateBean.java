package io.cloudocs.cloudocsapi.bean;

import io.cloudocs.cloudocsapi.model.Attribute;
import org.bson.types.ObjectId;

import java.util.List;

public class TemplateBean {

    private String id;
    private List<Attribute> attributes;
    private String url;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
