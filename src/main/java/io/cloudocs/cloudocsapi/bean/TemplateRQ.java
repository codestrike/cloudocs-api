package io.cloudocs.cloudocsapi.bean;

import io.cloudocs.cloudocsapi.model.Attribute;

import java.util.List;

/**
 * Created by javofegus on 4/11/17.
 */
public class TemplateRQ {
    private String documentId;
    private String url;
    private String name;
    private List<Attribute> attributes;

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }
}
