package io.cloudocs.cloudocsapi.bean;

public class TemplateRS {

    private StoragePath storagePath;

    public StoragePath getStoragePath() {
        return storagePath;
    }

    public void setStoragePath(StoragePath storagePath) {
        this.storagePath = storagePath;
    }
}
