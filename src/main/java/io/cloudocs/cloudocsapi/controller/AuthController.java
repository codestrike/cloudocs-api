package io.cloudocs.cloudocsapi.controller;

import io.cloudocs.cloudocsapi.bean.AuthenticateRQ;
import io.cloudocs.cloudocsapi.dao.UserDAO;
import io.cloudocs.cloudocsapi.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by wmartinez on 11/4/17.
 */
@RestController
@RequestMapping("auth")
public class AuthController {

    @Autowired
    private UserDAO repository;

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<User> create(@RequestBody AuthenticateRQ authenticateRQ) {
        User user = new User();

        try {
            user = repository.authenticate(authenticateRQ.getEmail(), authenticateRQ.getPassword());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public ResponseEntity<String> destroy(@RequestParam String authToken) {
        try {
            repository.logout(authToken);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return new ResponseEntity<>("", HttpStatus.OK);
    }


}
