package io.cloudocs.cloudocsapi.controller;

import io.cloudocs.cloudocsapi.bean.DestinationRS;
import io.cloudocs.cloudocsapi.bean.DispatcherRQ;
import io.cloudocs.cloudocsapi.bean.DocumentBean;
import io.cloudocs.cloudocsapi.model.Destination;
import io.cloudocs.cloudocsapi.service.DestinationService;
import io.cloudocs.cloudocsapi.service.DocumentService;
import io.cloudocs.cloudocsapi.transfer.DocumentTransfer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.swing.*;
import java.util.List;

@RestController
@RequestMapping("api")
public class DocumentController {

    @Autowired
    private DocumentService documentService;

    @Autowired
    private DestinationService destinationService;

    @RequestMapping(value = "/document" , method = RequestMethod.POST)
    public ResponseEntity<DocumentBean> saveDocument(@RequestBody DispatcherRQ dispatcherRQ) {

        DocumentTransfer documentTransfer = new DocumentTransfer();

        DocumentBean documentBean = documentService.saveDocument(dispatcherRQ.getDocument());

        Destination destination = new Destination();
        destination.setDestinationCode((dispatcherRQ.getDestinationCode()));
        destination.setDependence(documentBean.getDependence());
        destination.setDependenceCode(documentBean.getDependenceCode());

        destination.setDocument(documentTransfer.tranferToModel(documentBean));
        destinationService.saveDestination(destination);

        return new ResponseEntity<>(documentBean, HttpStatus.OK);
    }

    @RequestMapping(value = "/documents", method = RequestMethod.GET , params = {"id"},produces = "application/json;charset=UTF-8")
    public  ResponseEntity<List<DocumentBean>> getDocuments(@RequestParam("id") String id){
        List<DocumentBean> list = documentService.getDocumentsBySenderId(id);

        return new ResponseEntity<List<DocumentBean>>(list,HttpStatus.OK);
    }

    @RequestMapping(value = "/destinations", method = RequestMethod.GET, params = {"id"}, produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<DestinationRS>> getDestinations(@RequestParam ("id") String id){

        List<DestinationRS> list = destinationService.getDocumentsByDestination(id);

        return new ResponseEntity<List<DestinationRS>>(list,HttpStatus.OK);
    }


}
