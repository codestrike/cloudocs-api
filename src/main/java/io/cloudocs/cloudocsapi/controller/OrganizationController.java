package io.cloudocs.cloudocsapi.controller;

import io.cloudocs.cloudocsapi.dao.OrganizationDAO;
import io.cloudocs.cloudocsapi.model.Organization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by wmartinez on 11/4/17.
 */
@RestController
@RequestMapping("organizations")
public class OrganizationController {

    @Autowired
    private OrganizationDAO repository;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> index(@RequestParam(value = "slug", defaultValue = "all") String slug,
                                   @RequestParam(value = "ruc", defaultValue = "") String ruc) {
        if (!slug.equals("all")) {
            return ResponseEntity.ok(repository.findBySlug(slug));
        }

        if (!ruc.equals("")) {
            return ResponseEntity.ok(repository.findByRUC(ruc));
        }

        return ResponseEntity.ok(repository.findAll());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Organization> show(@PathVariable String id) {
        Organization organization = repository.findById(id);
        if (organization != null) {
            return new ResponseEntity<>(organization, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "", headers = {"Content-Type=application/json"}, method = RequestMethod.POST)
    public ResponseEntity<Organization> create(@RequestBody Organization organization) {
        repository.save(organization);
        return new ResponseEntity<>(organization, HttpStatus.CREATED);
    }

}
