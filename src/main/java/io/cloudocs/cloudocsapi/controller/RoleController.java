package io.cloudocs.cloudocsapi.controller;

import io.cloudocs.cloudocsapi.dao.OrganizationDAO;
import io.cloudocs.cloudocsapi.dao.RoleDAO;
import io.cloudocs.cloudocsapi.model.Organization;
import io.cloudocs.cloudocsapi.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by wmartinez on 11/4/17.
 */
@RestController
@RequestMapping("roles")
public class RoleController {

    @Autowired
    private RoleDAO repository;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> index() {
        return ResponseEntity.ok(repository.findAll());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Role> show(@PathVariable String id) {
        Role role = repository.findById(id);
        if (role != null) {
            return new ResponseEntity<>(role, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "", headers = {"Content-Type=application/json"}, method = RequestMethod.POST)
    public ResponseEntity<Role> create(@RequestBody Role role) {
        repository.save(role);
        return new ResponseEntity<>(role, HttpStatus.CREATED);
    }
}
