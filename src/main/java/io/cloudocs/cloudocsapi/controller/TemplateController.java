package io.cloudocs.cloudocsapi.controller;

import io.cloudocs.cloudocsapi.bean.TemplateBean;
import io.cloudocs.cloudocsapi.model.Template;
import io.cloudocs.cloudocsapi.service.TemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;


@Controller
@RequestMapping("api")
public class TemplateController {

    @Autowired
    TemplateService templateService;

    @RequestMapping(value = "/templates", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TemplateBean>> getTemplates(){
            List<TemplateBean> templateList = templateService.getTemplates();

            return new ResponseEntity<>(templateList, HttpStatus.OK);

    }
}
