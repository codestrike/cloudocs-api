package io.cloudocs.cloudocsapi.controller;

import io.cloudocs.cloudocsapi.dao.UserDAO;
import io.cloudocs.cloudocsapi.model.User;
import io.cloudocs.cloudocsapi.util.Encryptor;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by wmartinez on 11/4/17.
 */
@RestController
@RequestMapping("users")
public class UserController {

    @Autowired
    private UserDAO repository;

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<User> create(@RequestBody User user) {
        Encryptor encryptor = new Encryptor();
        String password = encryptor.cryptWithMD5(user.getPassword());
        user.setPassword(password);
        repository.save(user);
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/getusers", method = RequestMethod.GET)
    public ResponseEntity<List<User>> create() {
        List<User> list = repository.findAll();

        return new ResponseEntity<>(list, HttpStatus.CREATED);
    }

}
