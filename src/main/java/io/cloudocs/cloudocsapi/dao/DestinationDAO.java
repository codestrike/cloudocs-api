package io.cloudocs.cloudocsapi.dao;

import io.cloudocs.cloudocsapi.model.Destination;

import java.util.List;

public interface DestinationDAO {

    Destination saveDestination(Destination destination);
    List<Destination> getDocumentsByDestination(String destinationCode);
}
