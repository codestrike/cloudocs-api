package io.cloudocs.cloudocsapi.dao;

import io.cloudocs.cloudocsapi.model.Document;

import java.util.List;

public interface DocumentDAO {

    Document saveDocument(Document document);

    List<Document> getDocumentsBySenderId(String id);
}
