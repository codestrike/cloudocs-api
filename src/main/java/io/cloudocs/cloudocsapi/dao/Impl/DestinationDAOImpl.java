package io.cloudocs.cloudocsapi.dao.Impl;

import io.cloudocs.cloudocsapi.dao.DestinationDAO;
import io.cloudocs.cloudocsapi.model.Destination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DestinationDAOImpl implements DestinationDAO{

    @Autowired
    MongoOperations mongoOperations;

    @Override
     public Destination saveDestination(Destination destination) {
        try {
            mongoOperations.save(destination);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return destination;
    }

    @Override
    public List<Destination> getDocumentsByDestination(String destinationCode) {
        Query query = new Query();

        query.addCriteria(Criteria.where("destinationCode").is(destinationCode));

        List<Destination> list = mongoOperations.find(query, Destination.class);

        return list;
    }
}
