package io.cloudocs.cloudocsapi.dao.Impl;

import io.cloudocs.cloudocsapi.dao.DocumentDAO;
import io.cloudocs.cloudocsapi.model.Destination;
import io.cloudocs.cloudocsapi.model.Document;
import io.cloudocs.cloudocsapi.model.Sender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DocumentDAOImpl implements DocumentDAO {

    @Autowired
    MongoOperations mongoOperations;

    @Override
    public Document saveDocument(Document document) {
        try {
            mongoOperations.save(document);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return document;
    }

    @Override
    public List<Document> getDocumentsBySenderId(String id) {
        Query query = new Query();
        Sender sender = new Sender();
        sender.setCode(id);
        query.addCriteria(Criteria.where("sender.code").is(id));

        List<Document> list = mongoOperations.find(query, Document.class);

        return list;
    }
}
