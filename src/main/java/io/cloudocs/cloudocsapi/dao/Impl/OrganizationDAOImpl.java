package io.cloudocs.cloudocsapi.dao.Impl;

import io.cloudocs.cloudocsapi.dao.OrganizationDAO;
import io.cloudocs.cloudocsapi.model.Organization;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

/**
 * Created by wmartinez on 11/4/17.
 */
@Repository
public class OrganizationDAOImpl implements OrganizationDAO {

    @Autowired
    MongoOperations mongoOperations;

    @Override
    public List<Organization> findAll() {
        List<Organization> organizations = Collections.emptyList();
        try {
            organizations = mongoOperations.findAll(Organization.class);
        }catch (Exception e){
            e.printStackTrace();
        }
        return organizations;
    }

    @Override
    public Organization findById(String id) {
        ObjectId idOrganization = new ObjectId(id);
        return mongoOperations.findById(idOrganization, Organization.class);
    }

    @Override
    public Organization findByRUC(String ruc) {
        Organization organization = new Organization();
        Query query = new Query();
        query.addCriteria(Criteria.where("ruc").is(ruc));

        try {
            organization = mongoOperations.findOne(query, Organization.class);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return organization;
    }

    @Override
    public Organization findBySlug(String slug) {
        Organization organization = new Organization();
        Query query = new Query();
        query.addCriteria(Criteria.where("slug").is(slug.toLowerCase()));

        try {
            organization = mongoOperations.findOne(query, Organization.class);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return organization;
    }

    @Override
    public void save(Organization organization) {
        try {
            mongoOperations.save(organization);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
