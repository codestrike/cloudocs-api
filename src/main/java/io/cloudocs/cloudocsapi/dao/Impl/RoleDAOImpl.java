package io.cloudocs.cloudocsapi.dao.Impl;

import io.cloudocs.cloudocsapi.dao.RoleDAO;
import io.cloudocs.cloudocsapi.model.Role;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

/**
 * Created by wmartinez on 11/4/17.
 */
@Repository
public class RoleDAOImpl implements RoleDAO {

    @Autowired
    MongoOperations mongoOperations;

    @Override
    public List<Role> findAll() {
        List<Role> roles = Collections.emptyList();
        try {
            roles = mongoOperations.findAll(Role.class);
        }catch (Exception e){
            e.printStackTrace();
        }
        return roles;
    }

    @Override
    public Role findById(String id) {
        ObjectId idRole = new ObjectId(id);
        return mongoOperations.findById(idRole, Role.class);
    }

    @Override
    public void save(Role role) {
        try {
            mongoOperations.save(role);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
