package io.cloudocs.cloudocsapi.dao.Impl;

import io.cloudocs.cloudocsapi.dao.TemplateDAO;
import io.cloudocs.cloudocsapi.model.Template;
import org.bson.types.ObjectId;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;

import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;
import org.springframework.stereotype.Repository;

@Repository
public class TemplateDAOImpl implements TemplateDAO {

    private final static Logger logger = LoggerFactory.getLogger(TemplateDAOImpl.class);

    @Autowired
    MongoOperations mongoOperations;

    @Override
    public List<Template> getTemplates() {
        List<Template> list = Collections.emptyList();
        try {
            list = mongoOperations.findAll(Template.class);
        }catch (Exception e){
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public void saveTemplate(Template template) {
        try {
            mongoOperations.save(template);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Template getTemplateById(String id) {
        ObjectId idTemplate = new ObjectId(id);
        return mongoOperations.findById(idTemplate, Template.class);
    }
}
