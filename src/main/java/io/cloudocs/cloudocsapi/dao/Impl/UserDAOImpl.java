package io.cloudocs.cloudocsapi.dao.Impl;

import io.cloudocs.cloudocsapi.dao.UserDAO;
import io.cloudocs.cloudocsapi.model.User;
import io.cloudocs.cloudocsapi.util.Encryptor;
import io.cloudocs.cloudocsapi.util.TokenGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

/**
 * Created by wmartinez on 11/4/17.
 */
@Repository
public class UserDAOImpl implements UserDAO {

    @Autowired
    MongoOperations mongoOperations;

    @Override
    public User authenticate(String usernameOrEmail, String password) {
        User user = new User();
        Encryptor encryptor = new Encryptor();
        Query query = new Query();
        query.addCriteria(Criteria.where("email").is(usernameOrEmail));
        query.addCriteria(Criteria.where("password").is(encryptor.cryptWithMD5(password)));

        try {
            user = mongoOperations.findOne(query, User.class);

            if (user != null) {
                String authToken = new TokenGenerator().generateToken(user.getEmail());
                user.setAuthToken(authToken);
                mongoOperations.save(user);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return user;
    }

    @Override
    public User findByEmail(String email) {
        User user = new User();
        Query query = new Query();
        query.addCriteria(Criteria.where("email").is(email));

        try {
            user = mongoOperations.findOne(query, User.class);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return user;
    }

    @Override
    public List<User> findAll() {
        List<User> users = Collections.emptyList();
        try {
            users = mongoOperations.findAll(User.class);
        }catch (Exception e){
            e.printStackTrace();
        }
        return users;
    }

    @Override
    public void save(User user) {
        try {
            mongoOperations.save(user);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void logout(String authToken) {
        User user;
        Query query = new Query();
        query.addCriteria(Criteria.where("authToken").is(authToken));

        try {
            user = mongoOperations.findOne(query, User.class);
            if (user != null) {
                String newAuthToken = new TokenGenerator().generateToken(user.getEmail());
                user.setAuthToken(newAuthToken);
                mongoOperations.save(user);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
