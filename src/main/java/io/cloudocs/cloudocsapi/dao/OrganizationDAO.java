package io.cloudocs.cloudocsapi.dao;

import io.cloudocs.cloudocsapi.model.Organization;

import java.util.List;

/**
 * Created by wmartinez on 11/4/17.
 */
public interface OrganizationDAO {

    List<Organization> findAll();
    Organization findById(String id);
    Organization findByRUC(String ruc);
    Organization findBySlug(String slug);

    void save(Organization organization);

}
