package io.cloudocs.cloudocsapi.dao;

import io.cloudocs.cloudocsapi.model.Organization;
import io.cloudocs.cloudocsapi.model.Role;

import java.util.List;

/**
 * Created by wmartinez on 11/4/17.
 */
public interface RoleDAO {

    List<Role> findAll();
    Role findById(String id);
    void save(Role role);

}
