package io.cloudocs.cloudocsapi.dao;

import io.cloudocs.cloudocsapi.model.Template;

import java.util.List;

public interface TemplateDAO {

    List<Template> getTemplates();

    void saveTemplate(Template template);

    Template getTemplateById(String id);
}
