package io.cloudocs.cloudocsapi.dao;

import io.cloudocs.cloudocsapi.model.User;

import java.util.List;

/**
 * Created by wmartinez on 11/4/17.
 */
public interface UserDAO {

    User authenticate(String usernameOrEmail, String password);
    User findByEmail(String email);
    List<User> findAll();

    void save(User user);

    void logout(String authToken);

}
