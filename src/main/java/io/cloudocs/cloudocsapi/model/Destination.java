package io.cloudocs.cloudocsapi.model;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.DBRef;

import javax.print.Doc;

@org.springframework.data.mongodb.core.mapping.Document
public class Destination {

    private ObjectId id;
    private String destinationCode;
    @DBRef
    private Document document;
    private String type;
    private String dependenceCode;
    private String dependence;

    public String getDestinationCode() {
        return destinationCode;
    }

    public void setDestinationCode(String destinationCode) {
        this.destinationCode = destinationCode;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDependenceCode() {
        return dependenceCode;
    }

    public void setDependenceCode(String dependenceCode) {
        this.dependenceCode = dependenceCode;
    }

    public String getDependence() {
        return dependence;
    }

    public void setDependence(String dependence) {
        this.dependence = dependence;
    }
}
