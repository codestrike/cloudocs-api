package io.cloudocs.cloudocsapi.model;

import org.springframework.data.annotation.Id;

/**
 * Created by wmartinez on 11/4/17.
 */
public class Role {

    @Id
    public String id;
    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
