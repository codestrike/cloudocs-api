package io.cloudocs.cloudocsapi.service;

import io.cloudocs.cloudocsapi.bean.DestinationRS;
import io.cloudocs.cloudocsapi.model.Destination;

import java.util.List;

public interface DestinationService {

    Destination saveDestination(Destination destination);
    List<DestinationRS> getDocumentsByDestination(String destinationCode);

}
