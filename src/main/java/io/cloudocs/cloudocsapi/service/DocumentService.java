package io.cloudocs.cloudocsapi.service;

import io.cloudocs.cloudocsapi.bean.DocumentBean;
import io.cloudocs.cloudocsapi.model.Document;

import java.util.List;

public interface DocumentService {

    DocumentBean saveDocument(Document document);

    List<DocumentBean> getDocumentsBySenderId(String id);
}
