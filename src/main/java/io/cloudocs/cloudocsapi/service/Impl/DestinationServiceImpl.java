package io.cloudocs.cloudocsapi.service.Impl;

import io.cloudocs.cloudocsapi.bean.DestinationRS;
import io.cloudocs.cloudocsapi.dao.DestinationDAO;
import io.cloudocs.cloudocsapi.model.Destination;
import io.cloudocs.cloudocsapi.service.DestinationService;
import io.cloudocs.cloudocsapi.transfer.DestinationTransfer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DestinationServiceImpl implements DestinationService {
    DestinationTransfer transfer = new DestinationTransfer();

    @Autowired
    DestinationDAO destinationDAO;


    @Override
    public Destination saveDestination(Destination destination) {
        return destinationDAO.saveDestination(destination) ;
    }

    @Override
    public List<DestinationRS> getDocumentsByDestination(String destinationCode) {
        List<Destination> destinations = destinationDAO.getDocumentsByDestination(destinationCode);

        List<DestinationRS> list = transfer.getDestinations(destinations);

        return list;
    }
}
