package io.cloudocs.cloudocsapi.service.Impl;

import io.cloudocs.cloudocsapi.api.FileManagerClient;
import io.cloudocs.cloudocsapi.bean.DocumentBean;
import io.cloudocs.cloudocsapi.bean.TemplateRQ;
import io.cloudocs.cloudocsapi.bean.TemplateRS;
import io.cloudocs.cloudocsapi.dao.DocumentDAO;
import io.cloudocs.cloudocsapi.model.Document;
import io.cloudocs.cloudocsapi.service.DocumentService;
import io.cloudocs.cloudocsapi.transfer.DocumentTransfer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class DocumentServiceImpl implements DocumentService{

    DocumentTransfer transfer = new DocumentTransfer();

    private DocumentDAO documentDAO;
    private FileManagerClient fileManagerClient;

    @Autowired
    public DocumentServiceImpl(DocumentDAO documentDAO, FileManagerClient fileManagerClient) {
        this.documentDAO = documentDAO;
        this.fileManagerClient = fileManagerClient;
    }

    @Override
    public DocumentBean saveDocument(Document document) {

        document.setFecha(new Date());

        DocumentBean documentBean = transfer.tranferToBean(documentDAO.saveDocument(document));

        TemplateRQ templateRQ = new TemplateRQ();
        templateRQ.setDocumentId(documentBean.getId());
        templateRQ.setUrl(document.getTemplate().getUrl());
        templateRQ.setName(document.getTemplate().getName());
        templateRQ.setAttributes(document.getTemplate().getAttributes());

        TemplateRS templateRS = fileManagerClient.generate(templateRQ);
        documentBean.setFileName(templateRS.getStoragePath().getParentPath() +
                        templateRS.getStoragePath().getPdfFile());

        return documentBean;
    }

    @Override
    public List<DocumentBean> getDocumentsBySenderId(String id) {
        List<Document> documents = documentDAO.getDocumentsBySenderId(id);
        List<DocumentBean> list = new ArrayList<>();
        for(Document document : documents){
            DocumentBean bean = transfer.tranferToBean(document);
            list.add(bean);
        }
        return list;
    }
}
