package io.cloudocs.cloudocsapi.service.Impl;

import io.cloudocs.cloudocsapi.bean.TemplateBean;
import io.cloudocs.cloudocsapi.dao.TemplateDAO;
import io.cloudocs.cloudocsapi.model.Template;
import io.cloudocs.cloudocsapi.service.TemplateService;
import io.cloudocs.cloudocsapi.transfer.TemplateTransfer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TemplateServiceImpl implements TemplateService {

    TemplateTransfer templateTransfer = new TemplateTransfer();

    @Autowired
    TemplateDAO templateDAO;

    @Override
    public List<TemplateBean> getTemplates() {
        List<TemplateBean> list = templateTransfer.tranferListToBean(templateDAO.getTemplates());

        return list;
    }

    @Override
    public Template getTemplate() {
        return null;
    }

    @Override
    public Template getTemplateById(String id) {
        return templateDAO.getTemplateById(id);
    }

    @Override
    public void saveTemplate(Template template) {
        templateDAO.saveTemplate(template);
    }
}
