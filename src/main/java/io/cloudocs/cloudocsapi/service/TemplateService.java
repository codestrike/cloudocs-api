package io.cloudocs.cloudocsapi.service;

import io.cloudocs.cloudocsapi.bean.TemplateBean;
import io.cloudocs.cloudocsapi.model.Template;

import java.util.List;

public interface TemplateService {

    List<TemplateBean> getTemplates();
    Template getTemplate();
    Template getTemplateById(String id);
    void saveTemplate(Template template);
}
