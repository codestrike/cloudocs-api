package io.cloudocs.cloudocsapi.transfer;

import io.cloudocs.cloudocsapi.bean.DestinationRS;
import io.cloudocs.cloudocsapi.model.Destination;

import java.util.ArrayList;
import java.util.List;

public class DestinationTransfer {
    public List<DestinationRS> getDestinations(List<Destination> destinations) {
        List<DestinationRS> list = new ArrayList<>();

        for(Destination destination : destinations){
            DestinationRS bean = getDestination(destination);
            list.add(bean);
        }
        return  list;
    }

    private DestinationRS getDestination(Destination destination) {
        DestinationRS bean = new DestinationRS();
        bean.setId(destination.getId().toString());
        bean.setDependence(destination.getDependence());
        bean.setDependenceCode(destination.getDependenceCode());
        bean.setDocument(destination.getDocument());
        bean.setType(destination.getType());

        return  bean;
    }
}
