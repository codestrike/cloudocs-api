package io.cloudocs.cloudocsapi.transfer;

import io.cloudocs.cloudocsapi.bean.DocumentBean;
import io.cloudocs.cloudocsapi.bean.TemplateBean;
import io.cloudocs.cloudocsapi.model.Document;
import io.cloudocs.cloudocsapi.model.Template;
import org.bson.types.ObjectId;

public class DocumentTransfer {

    public DocumentBean tranferToBean(Document document){
        DocumentBean bean = new DocumentBean();
        bean.setId(document.getId().toString());
        bean.setCode(document.getCode());
        bean.setDependence(document.getDependence());
        bean.setDependenceCode(document.getDependenceCode());
        bean.setFileName(document.getFileName());
        bean.setFecha(document.getFecha());
        bean.setSender(document.getSender());
        bean.setState(document.getState());
        bean.setSubject(document.getSubject());
        TemplateBean templateBean = new TemplateBean();
        templateBean.setId(document.getTemplate().getId().toString());
        templateBean.setName(document.getTemplate().getName());
        templateBean.setUrl(document.getTemplate().getUrl());
        templateBean.setAttributes(document.getTemplate().getAttributes());
        bean.setTemplateBean(templateBean);
        bean.setType(document.getType());

        return bean;
    }

    public Document tranferToModel(DocumentBean documentBean) {

        Document bean = new Document();
        bean.setId(new ObjectId(documentBean.getId()));
        bean.setCode(documentBean.getCode());
        bean.setDependence(documentBean.getDependence());
        bean.setDependenceCode(documentBean.getDependenceCode());
        bean.setFileName(documentBean.getFileName());
        bean.setFecha(documentBean.getFecha());
        bean.setSender(documentBean.getSender());
        bean.setState(documentBean.getState());
        bean.setSubject(documentBean.getSubject());

        Template template = new Template();
        template.setId(new ObjectId(documentBean.getTemplateBean().getId()));
        template.setName(documentBean.getTemplateBean().getName());
        template.setUrl(documentBean.getTemplateBean().getUrl());
        template.setAttributes(documentBean.getTemplateBean().getAttributes());
        bean.setTemplate(template);
        bean.setType(documentBean.getType());

        return bean;
    }
}
