package io.cloudocs.cloudocsapi.transfer;

import io.cloudocs.cloudocsapi.bean.TemplateBean;
import io.cloudocs.cloudocsapi.model.Template;

import java.util.ArrayList;
import java.util.List;

public class TemplateTransfer {

    public List<TemplateBean> tranferListToBean(List<Template> templates){
        List<TemplateBean> list = new ArrayList<>();

        for(Template template : templates ){
            TemplateBean templateBean = tranferToBean(template);
            list.add(templateBean);
        }
        return list;
    }

    public TemplateBean tranferToBean(Template template){
        TemplateBean templateBean = new TemplateBean();

        templateBean.setId(template.getId().toString());
        templateBean.setUrl(template.getUrl());
        templateBean.setAttributes(template.getAttributes());
        templateBean.setName(template.getName());

        return templateBean;
    }
}
