package io.cloudocs.cloudocsapi;

import io.cloudocs.cloudocsapi.bean.TemplateBean;
import io.cloudocs.cloudocsapi.model.Attribute;
import io.cloudocs.cloudocsapi.model.Template;
import io.cloudocs.cloudocsapi.service.TemplateService;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CloudocsApiApplicationTests {

	@Autowired
	TemplateService templateService;

	@Ignore
	@Test
	public void ShouldSaveTemplate(){

		Template template = new Template();
		template.setName("Informe");
		List<Attribute> attributes = new ArrayList<>();
		Attribute attribute = new Attribute();
		attribute.setLabel("Motivo");
		attribute.setName("type");
		attribute.setType("STRING");
		attributes.add(attribute);

		attribute = new Attribute();
		attribute.setLabel("Encargado");
		attribute.setName("attentant");
		attribute.setType("STRING");
		attributes.add(attribute);

		attribute = new Attribute();
		attribute.setLabel("Detalle");
		attribute.setName("detail");
		attribute.setType("TEXT");
		attributes.add(attribute);

		template.setAttributes(attributes);
		template.setUrl("C:/solicitud");

		templateService.saveTemplate(template);

	}
	@Ignore
	@Test
	public void ShouldListTemplates(){
		List<TemplateBean> templates = templateService.getTemplates();

	}
	@Ignore
	@Test
	public void ShouldGetTemplateById(){

		Template template = templateService.getTemplateById("59fdb10bd5f36f20c07a645f");
	}

}
