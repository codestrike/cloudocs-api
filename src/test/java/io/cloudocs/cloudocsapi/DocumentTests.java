package io.cloudocs.cloudocsapi;


import io.cloudocs.cloudocsapi.bean.DocumentBean;
import io.cloudocs.cloudocsapi.model.Document;
import io.cloudocs.cloudocsapi.model.Sender;
import io.cloudocs.cloudocsapi.model.Template;
import io.cloudocs.cloudocsapi.service.DocumentService;
import org.bson.types.ObjectId;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DocumentTests {

    @Autowired
    DocumentService documentService;

    @Ignore
    @Test
    public void ShouldSaveDocument(){
        Document document = new Document();

        document.setCode("2017-0001498474");
        document.setType("ADMINISTRATIVO");
        document.setState("ARCHIVADO");
        document.setDependence("211");
        document.setDependence("JEFATURA DE AREA DE ATENCION AL CIUDADANO Y TRAMITE DOCUMENTARIO");
        Sender sender = new Sender();
        sender.setCode("001");
        sender.setName("ANDRE BAZALAR");
        sender.setType("ADMINISTRATIVO");
        document.setSender(sender);
        document.setFecha(new Date());
        document.setSubject("SOLICITUD REEMBOLSO DE VIATICO ");

        Template template = new Template();
        template.setId(new ObjectId("59fdb10bd5f36f20c07a645f"));
        template.setName("document-prueba");
        template.setType("INFORME");

        document.setTemplate(template);

        DocumentBean bean = documentService.saveDocument(document);

    }

}
